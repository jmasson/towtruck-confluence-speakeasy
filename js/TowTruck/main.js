/**
 * The main module
 *
 * @context atl.general
 */
var $ = require('speakeasy/jquery').jQuery;
var img = require('speakeasy/resources').getImageUrl(module, 'projectavatar.png');

$(document).ready(function() {
    jQuery.getScript("https://towtruck.mozillalabs.com/towtruck.js");

  
    $('<li class="ajs-button normal"><a href="#" onclick=\"TowTruck(this); return false;\" id=towtruck><span> TowTruck</span></a></li>').prependTo('#navigation > .ajs-menu-bar');
  
});